const { UserRepository } = require('../repositories/userRepository');

class UserService {

	// TODO: Implement methods to work with user

	search(search) {
		toLowerCaseObjData(search);
		const item = UserRepository.getOne(search);
		if(!item) {
			return null;
		}
		return item;
	}

	all() {
		return UserRepository.getAll();
	}

	create(data) {
		if (!data) return null;
		toLowerCaseObjData(data);
		const { email, phoneNumber } = data;
		const userEmailRepeat = this.search({ email });
		const userPhoneRepeat = this.search({ phoneNumber });
		if (data && !userEmailRepeat && !userPhoneRepeat) {
			return UserRepository.create(data);
		}
		return null;
	}

	update(id, data) {
		if (!id || !data || !this.search({ id })) return null;
		toLowerCaseObjData(data);

		let ok = true;
		const dataKeys = Object.keys(data);
		dataKeys.forEach((key) => {
			if (key === 'email' || key === 'phoneNumber') {
				const opt = {};
				opt[key] = data[key];
				const userRepeat = this.search(opt);
				if (userRepeat) {
					ok = false;
					return;
				}

			}
		});

		return ok ? UserRepository.update(id, data) : null;
	}

	delete(id) {
		if (!id) return null;
		const res = UserRepository.delete(id);
		return res.length > 0 ? res[0] : null;
	}
}

function toLowerCaseObjData(data) {
	Object.keys(data).forEach((key) => {
		if (!isNaN(data[key])) {
			data[key] = Number.parseInt(data[key]);
		}
	});
}

module.exports = new UserService();