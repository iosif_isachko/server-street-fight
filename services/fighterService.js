const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
	// TODO: Implement methods to work with fighters

	search(search) {
		toLowerCaseObjData(search);
		const item = FighterRepository.getOne(search);
		if(!item) {
			return null;
		}
		return item;
	}

	all() {
		return FighterRepository.getAll();
	}

	create(data) {
		if (!data) return null;
		toLowerCaseObjData(data);
		const { name } = data;
		const fighterNameRepeat = this.search({ name });
		if (!fighterNameRepeat) {
			if (!data.health) {
				data.health = 100;
			}
			return FighterRepository.create(data);
		}
		return null;
	}

	update(id, data) {
		if (!id || !data || !this.search({ id })) return null;
		toLowerCaseObjData(data);

		let ok = true;
		const dataKeys = Object.keys(data);
		dataKeys.forEach((key) => {
			if (key === 'name') {
				const opt = {};
				opt[key] = data[key];
				const fighterRepeat = this.search(opt);
				if (fighterRepeat) {
					ok = false;
					return;
				}

			}
		});

		return ok ? FighterRepository.update(id, data) : null;
	}

	delete(id) {
		if (!id) return null;
		const res = FighterRepository.delete(id);
		return res.length > 0 ? res[0] : null;
	}
}

function toLowerCaseObjData(data) {
	Object.keys(data).forEach((key) => {
		if (!isNaN(data[key])) {
			data[key] = Number.parseInt(data[key]);
		}
	});
}

module.exports = new FighterService();