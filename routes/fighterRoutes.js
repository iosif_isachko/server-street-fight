const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');
const { createReport, results } = require('../middlewares/helper.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

/*
FIGHTER
	GET /api/fighters
	GET /api/fighters/:id
	POST /api/fighters
	PUT /api/fighters/:id
	DELETE /api/fighters/:id
*/

// GET ALL FIGHTERS
router.get(
	'/',
	(req, res, next) => {
		const fighters = FighterService.all();
		res[results] = fighters
			? createReport(fighters)
			: createReport(null, 'Fighters not found.', 404);
		next();
	},
	responseMiddleware,
);

// GET FIGHTER BY ID
router.get(
	'/:id',
	(req, res, next) => {
		const { id } = req.params;
		const fighter = FighterService.search({ id });
		res[results] = fighter
			? createReport(fighter)
			: createReport(null, 'Fighter not found.', 404);
		next();
	},
	responseMiddleware,
);

// CREATE FIGHTER
router.post(
	'/',
	createFighterValid,
	(req, res, next) => {
		if (!res[results].error && res[results].data) {
			const newFighter = FighterService.create(res[results].data);
			res[results] = newFighter
				? createReport(newFighter)
				: createReport(null, 'Perhaps such a fighter already exists.', 400);
		}
		next();
	},
	responseMiddleware,
);

// UPDATE FIGHTER BY ID
router.put(
	'/:id',
	updateFighterValid,
	(req, res, next) => {
		if (!res[results].error && res[results].data) {
			const { id } = req.params;
			const updateFighter = FighterService.update(id, res[results].data);
			res[results] = updateFighter
				? createReport(updateFighter)
				: createReport(null, 'Fighter not found or such data already exists.', 400);
		}
		next();
	},
	responseMiddleware,
);

// DELETE FIGHTER BY ID
router.delete(
	'/:id',
	(req, res, next) => {
		const { id } = req.params;
		const delFighter = FighterService.delete(id);
		res[results] = delFighter
			? createReport(delFighter)
			: createReport(null, `There is no fighter with this id: ${id}.`, 400);
		next();
	},
	responseMiddleware,
);

module.exports = router;