const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createReport, results } = require('../middlewares/helper.middleware');

const router = Router();

// TODO: Implement route controllers for user

/*
USER:
	GET /api/users
	GET /api/users/:id
	POST /api/users
	PUT /api/users/:id
	DELETE /api/users/:id
*/

// GET ALL USERS
router.get(
	'/',
	(req, res, next) => {
		const users = UserService.all();
		res[results] = users
			? createReport(users)
			: createReport(null, 'Users not found.', 404);
		next();
	},
	responseMiddleware,
);

// GET USER BY ID
router.get(
	'/:id',
	(req, res, next) => {
		const { id } = req.params;
		const user = UserService.search({ id });
		res[results] = user
			? createReport(user)
			: createReport(null, 'User not found.', 404);
		next();
	},
	responseMiddleware,
);

// CREATE USER
router.post(
	'/',
	createUserValid,
	(req, res, next) => {
		if (!res[results].error && res[results].data) {
			const newUser = UserService.create(res[results].data);
			res[results] = newUser
				? createReport(newUser)
				: createReport(null, 'Perhaps such a user already exists.', 400);
		}
		next();
	},
	responseMiddleware,
);

// UPDATE USER BY ID
router.put(
	'/:id',
	updateUserValid,
	(req, res, next) => {
		if (!res[results].error && res[results].data) {
			const { id } = req.params;
			const updateUser = UserService.update(id, res[results].data);
			res[results] = updateUser
				? createReport(updateUser)
				: createReport(null, 'User not found or such data already exists.', 400);
		}
		next();
	},
	responseMiddleware,
);

// DELETE USER BY ID
router.delete(
	'/:id',
	(req, res, next) => {
		const { id } = req.params;
		const delUser = UserService.delete(id);
		res[results] = delUser
			? createReport(delUser)
			: createReport(null, `There is no user with this id: ${id}.`, 400);
		next();
	},
	responseMiddleware,
);


module.exports = router;