const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createReport, results } = require('../middlewares/helper.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
	try {
		const { email, password } = req.body;
		if(!email || !password) { throw Error('Incorrect data.'); }
		const user = AuthService.login({ email, password });
		res[results] = createReport(user);
		res.data = user;
	} catch (err) {
		res[results] = createReport(null, err, 400);
		res.err = err;
	} finally {
		next();
	}
}, responseMiddleware);

module.exports = router;