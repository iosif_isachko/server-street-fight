const { user } = require('../models/user');
const {
	results,
	createReport,
	isString,
	//
	isGmail,
	isPhone,
} = require('./helper.middleware');

const userValid = {
	firstName: (data) => isString(data),
	lastName: (data) => isString(data),
	email: (data) => isGmail.test(data),
	phoneNumber: (data) => isPhone.test(data),
	password: (data) => isString(data),
};

const userFieldValid = (key, data) => userValid[key](data);


const createUserValid = (req, res, next) => {
	// TODO: Implement validation for user entity during creation
	const input = req.body;
	const userKeys = Object.keys(user);
	const inputKeys = Object.keys(input);

	let ok = true;
	let errorText = 'User entity to create isn\'t valid.';

	ok = !inputKeys.includes('id') && ok;
	ok = inputKeys.length === userKeys.length - 1 && ok;

	ok && inputKeys.forEach((key) => {
		if (!userKeys.includes(key) || !userFieldValid(key, input[key])) {
			ok = false;
			errorText += ` Incorrect field: ${key}.`;
			return;
		}
	});

	res[results] = ok
		? res[results] = createReport(input, null)
		: createReport(null, errorText, 400);

	next();
}

const updateUserValid = (req, res, next) => {
	// TODO: Implement validation for user entity during update
	const input = req.body;
	const userKeys = Object.keys(user);
	const inputKeys = Object.keys(input);

	let ok = true;
	let errorText = 'User entity to update isn\'t valid.';

	ok = !inputKeys.includes('id') && ok;
	ok = inputKeys.length > 0 && ok;
	ok && inputKeys.forEach((key) => {
		if (!userKeys.includes(key) || !userFieldValid(key, input[key])) {
			ok = false;
			errorText += ` Incorrect field: ${key}.`;
			return;
		}
	});

	res[results] = ok
		? res[results] = createReport(input, null)
		: createReport(null, errorText, 400);

	next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;