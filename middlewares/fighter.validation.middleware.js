const { fighter } = require('../models/fighter');
const {
	results,
	createReport,
	isString,
	isNumber,
} = require('./helper.middleware');

const fighterValid = {
	name: (data) => isString(data),
	health: (data) => isNumber(data, 80, 120),
	power: (data) => isNumber(data, 1, 100),
	defense: (data) => isNumber(data, 1, 10),
};

const fighterFieldValid = (key, data) => fighterValid[key](data);

const createFighterValid = (req, res, next) => {
	// TODO: Implement validation for fighter entity during create
	const input = req.body;
	const fighterKeys = Object.keys(fighter);
	const inputKeys = Object.keys(input);

	let ok = true;
	let errorText = 'Fighter entity to create isn\'t valid.';

	ok = !inputKeys.includes('id') && ok;
	ok = (inputKeys.length === fighterKeys.length - 1
			|| (!inputKeys.includes('health') && inputKeys.length === fighterKeys.length - 2)) && ok;
	ok && inputKeys.forEach((key) => {
		if (!fighterKeys.includes(key) || !fighterFieldValid(key, input[key])) {
			ok = false;
			errorText += ` Incorrect field: ${key}.`;
			return;
		}
	});

	res[results] = ok
		? res[results] = createReport(input, null)
		: createReport(null, errorText, 400);

	next();
}

const updateFighterValid = (req, res, next) => {
	// TODO: Implement validation for fighter entity during update
	const input = req.body;
	const fighterKeys = Object.keys(fighter);
	const inputKeys = Object.keys(input);

	let ok = true;
	let errorText = 'Fighter entity to update isn\'t valid.';

	ok = !inputKeys.includes('id') && ok;
	ok = inputKeys.length > 0 && ok;
	ok && inputKeys.forEach((key) => {
		if (!fighterKeys.includes(key) || !fighterFieldValid(key, input[key])) {
			ok = false;
			errorText += ` Incorrect field: ${key}.`;
			return;
		}
	});

	res[results] = ok
		? res[results] = createReport(input, null)
		: createReport(null, errorText, 400);

	next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;