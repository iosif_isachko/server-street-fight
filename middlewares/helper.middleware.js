const results = Symbol('results');

const isGmail = new RegExp(/^[^\W\d_]\w+@gmail\.com$/);
const isPhone = new RegExp(/^\+380\d{9}$/);
const isString = (str) => typeof str === 'string' && str.length > 2;
const isNumber = (num, min, max) => !isNaN(num) && num >= min && num <= max;

const createReport = (data = null, errorMess = null, status = 200) => {
	const report = { status };
	errorMess && (report.error = errorMess);
	data && ( report.data = data);
	return report;
};


exports.isGmail = isGmail;
exports.isPhone = isPhone;
exports.isString = isString;
exports.isNumber = isNumber;
exports.results = results;
exports.createReport = createReport;