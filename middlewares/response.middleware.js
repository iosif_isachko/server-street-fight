const { results } = require('./helper.middleware');

const responseMiddleware = (req, res, next) => {
	// TODO: Implement middleware that returns result of the query

	let content = {};
	const report = res[results] || null;

	const error = (mes, status = 400) => {
		res.statusCode = status;
		content.error = true;
		content.message = mes;
	};

	if (!report || !report.status) {
		error('An unhandled error on the server.');
	} else if (report.error) {
		error(report.error, report.status);
	} else if (report.data) {
		res.statusCode = report.status;
		content = report.data;
	} else {
		error('Not found.', 404);
	}

	res.setHeader('Content-Type', 'text/json');
	res.json(content);
}

exports.responseMiddleware = responseMiddleware;